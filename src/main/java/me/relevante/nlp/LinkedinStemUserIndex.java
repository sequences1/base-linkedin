package me.relevante.nlp;

import me.relevante.network.Linkedin;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LinkedinStemUserIndex")
public class LinkedinStemUserIndex extends AbstractNetworkStemUserIndex<Linkedin> implements NetworkStemUserIndex<Linkedin> {

    public LinkedinStemUserIndex(String stem,
                                 String userId,
                                 double score) {
        super(stem, userId, score);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

}
