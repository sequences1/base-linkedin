package me.relevante.nlp;

import me.relevante.model.LinkedinActionComment;
import me.relevante.model.LinkedinActionLike;
import me.relevante.model.LinkedinFullPost;
import me.relevante.model.LinkedinFullUser;
import me.relevante.model.LinkedinGroup;
import me.relevante.model.LinkedinSearchUser;
import me.relevante.model.LinkedinSignalExtractor;
import me.relevante.model.NetworkSignal;
import me.relevante.model.NetworkSignalExtractor;
import me.relevante.network.Linkedin;
import me.relevante.persistence.NetworkFindByIdsRepo;
import me.relevante.persistence.NetworkFullPostRepo;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class LinkedinSearchUserCreator extends AbstractNetworkSearchUserCreator<Linkedin, LinkedinFullUser, LinkedinFullPost, LinkedinSearchUser, LinkedinActionLike, LinkedinActionComment>
        implements NetworkSearchUserCreator<Linkedin, LinkedinSearchUser> {

    private final NetworkFindByIdsRepo groupRepo;

    @Autowired
    public LinkedinSearchUserCreator(NetworkSignalExtractor<Linkedin, LinkedinFullUser, LinkedinFullPost> signalExtractor,
                                     NetworkVectorizer<Linkedin> vectorizer,
                                     CrudRepository<LinkedinFullUser, String> fullUserRepo,
                                     NetworkFullPostRepo<LinkedinFullPost> fullPostRepo,
                                     NetworkPostActionRepo<Linkedin, LinkedinActionLike> likesRepo,
                                     NetworkPostActionRepo<Linkedin, LinkedinActionComment> commentsRepo,
                                     NetworkFindByIdsRepo<LinkedinGroup> groupRepo) {
        super(signalExtractor, vectorizer, fullUserRepo, fullPostRepo, likesRepo, commentsRepo);
        this.groupRepo = groupRepo;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    protected void addCustomSignals(LinkedinFullUser fullUser,
                                    List<NetworkSignal<Linkedin>> allSignals) {
        LinkedinSignalExtractor linkedinSignalExtractor = (LinkedinSignalExtractor) signalExtractor;
        List<LinkedinGroup> groups = groupRepo.findByIdIn(fullUser.getGroupIds());
        for (LinkedinGroup group : groups) {
            allSignals.addAll(linkedinSignalExtractor.extract(group, fullUser.getId()));
        }
    }

    @Override
    protected void addLikesAndCommentsToFullPosts(List<LinkedinActionLike> likes,
                                               List<LinkedinActionComment> comments,
                                               Map<String, LinkedinFullPost> fullPostMap) {
        for (LinkedinActionLike like : likes) {
            LinkedinFullPost fullPost = fullPostMap.get(like.getPostId());
            fullPost.getLikes().add(like);
        }
        for (LinkedinActionComment comment : comments) {
            LinkedinFullPost fullPost = fullPostMap.get(comment.getPostId());
            fullPost.getComments().add(comment);
        }
    }

    @Override
    protected LinkedinSearchUser createSearchUser(LinkedinFullUser fullUser, List<NetworkSignal<Linkedin>> signals) {
        LinkedinSearchUser searchUser = new LinkedinSearchUser(fullUser);
        searchUser.getSignals().addAll(signals);
        return searchUser;
    }
}
