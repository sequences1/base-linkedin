package me.relevante.nlp;

import me.relevante.model.LinkedinSignalBioDescription;
import me.relevante.model.LinkedinSignalBioHeadline;
import me.relevante.model.LinkedinSignalGroupMembership;
import me.relevante.model.LinkedinSignalPosition;
import me.relevante.model.LinkedinSignalPostAuthorship;
import me.relevante.model.LinkedinSignalPostComment;
import me.relevante.model.LinkedinSignalPostLike;
import me.relevante.model.LinkedinSignalShareAuthorship;
import me.relevante.model.LinkedinSignalSkill;
import me.relevante.network.Linkedin;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class LinkedinVectorizer extends AbstractNetworkVectorizer<Linkedin> implements NetworkVectorizer<Linkedin> {

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    protected Map<Class, Function<Double, Double>> createWeightFunctionsBySignal() {
        Map<Class, Function<Double, Double>> weightFunctionsBySignal = new HashMap<>();
        weightFunctionsBySignal.put(LinkedinSignalBioHeadline.class, n -> 10.0);
        weightFunctionsBySignal.put(LinkedinSignalBioDescription.class, n -> 5.0);
        weightFunctionsBySignal.put(LinkedinSignalPosition.class, n -> 5.0);
        weightFunctionsBySignal.put(LinkedinSignalSkill.class, n -> 5.0);

        weightFunctionsBySignal.put(LinkedinSignalPostAuthorship.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(LinkedinSignalShareAuthorship.class, n -> 4.167 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(LinkedinSignalPostLike.class, n -> 8.333 / (1 + Math.pow(5.0, -n)));
        weightFunctionsBySignal.put(LinkedinSignalPostComment.class, n -> 4.167 / (1 + Math.pow(5.0, -n)));

        weightFunctionsBySignal.put(LinkedinSignalGroupMembership.class, n -> 12.5 + (12.5 * Math.min(n, 5)) / 5.0);
        return weightFunctionsBySignal;
    }

}
