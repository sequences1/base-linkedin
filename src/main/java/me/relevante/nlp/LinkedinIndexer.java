package me.relevante.nlp;

import me.relevante.model.LinkedinSearchUser;
import me.relevante.network.Linkedin;
import me.relevante.persistence.NetworkIndexRepo;
import me.relevante.persistence.NetworkSearchUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LinkedinIndexer extends AbstractNetworkIndexer<Linkedin, LinkedinSearchUser, LinkedinStemUserIndex>
        implements NetworkIndexer<Linkedin> {

    @Autowired
    public LinkedinIndexer(final LinkedinSearchUserCreator searchUserCreator,
                           final LinkedinVectorizer vectorizer,
                           final NetworkIndexRepo<Linkedin, LinkedinStemUserIndex> indexRepo,
                           final NetworkSearchUserRepo<Linkedin, LinkedinSearchUser> searchUserRepo) {
        super(searchUserCreator, vectorizer, indexRepo, searchUserRepo);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    protected LinkedinStemUserIndex createIndexEntry(String stem, String userId, double score) {
        return new LinkedinStemUserIndex(stem, userId, score);
    }
}
