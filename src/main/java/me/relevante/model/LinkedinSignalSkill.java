package me.relevante.model;

import java.util.Date;

public class LinkedinSignalSkill extends AbstractLinkedinSignal implements NetworkSignal {

    public LinkedinSignalSkill() {
        super("BS", SignalCategory.SELF_DESCRIPTION);
    }

    public LinkedinSignalSkill(String skill,
                               String userId,
                               int signalIndex) {
        this();
        this.nlpAnalyzableContent = skill;
        this.relatedEntityId = String.valueOf(signalIndex);
        this.relatedUserId = userId;
        this.signalTimestamp = new Date();
    }
}
