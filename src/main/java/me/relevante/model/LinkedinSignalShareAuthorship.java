package me.relevante.model;

import java.util.Date;

public class LinkedinSignalShareAuthorship extends AbstractLinkedinSignal implements NetworkSignal {

    public LinkedinSignalShareAuthorship() {
        super("SA", SignalCategory.CONTENT_ACTIVITY);
    }

    public LinkedinSignalShareAuthorship(LinkedinPost post) {
        this();
        this.nlpAnalyzableContent = post.getNlpAnalyzableContent();
        this.relatedEntityId = post.getId();
        this.relatedUserId = post.getAuthorId();
        this.signalTimestamp = (post.getCreationTimestamp() != null) ? post.getCreationTimestamp() : new Date();
    }
}
