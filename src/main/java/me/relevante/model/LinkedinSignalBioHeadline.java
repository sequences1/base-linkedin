package me.relevante.model;

import java.util.Date;

public class LinkedinSignalBioHeadline extends AbstractLinkedinSignal implements NetworkSignal {

    public LinkedinSignalBioHeadline() {
        super("BH", SignalCategory.SELF_DESCRIPTION);
    }

    public LinkedinSignalBioHeadline(LinkedinProfile profile) {
        this();
        this.nlpAnalyzableContent = profile.getHeadline();
        this.relatedEntityId = profile.getId();
        this.relatedUserId = profile.getId();
        this.signalTimestamp = (profile.getUpdateTimestamp() != null) ? profile.getUpdateTimestamp() : new Date();
    }

}
