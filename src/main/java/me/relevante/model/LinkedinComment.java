package me.relevante.model;

import me.relevante.network.Linkedin;
import org.springframework.data.annotation.Transient;

import java.util.Date;

public class LinkedinComment extends AbstractNetworkPostAction<Linkedin> implements NetworkEntity<Linkedin> {

	private String comment;

	@Transient
	private LinkedinProfile author;

    public LinkedinComment() {
        super();
    }

    public LinkedinComment(String authorId,
                           String postId,
                           String comment) {
        super(authorId, postId);
		this.comment = comment;
	}

    public LinkedinComment(LinkedinProfile author,
                           String postId,
                           String comment) {
        this(author.getId(), postId, comment);
        setAuthor(author);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public String getPostId() {
        return postId;
    }

    public String getAuthorId() {
        return authorId;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date timestamp) {
        this.creationTimestamp = timestamp;
    }

    public String getComment() {
        return comment;
    }

    public LinkedinProfile getAuthor() {
        return author;
    }

    public void setAuthor(LinkedinProfile author) {
        this.author = author;
        this.authorId = author.getId();
    }

}
