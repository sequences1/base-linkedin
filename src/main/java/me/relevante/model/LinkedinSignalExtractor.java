package me.relevante.model;

import me.relevante.network.Linkedin;
import me.relevante.nlp.core.Keyword;
import me.relevante.nlp.core.NlpCore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class LinkedinSignalExtractor implements NetworkSignalExtractor<Linkedin, LinkedinFullUser, LinkedinFullPost> {

    @Autowired private NlpCore nlpCore;

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public List<NetworkSignal<Linkedin>> extract(LinkedinFullUser fullUser) {
        Set<NetworkSignal<Linkedin>> signals = new HashSet<>();

        List<NetworkSignal<Linkedin>> profileSignals = extract(fullUser.getProfile());
        signals.addAll(profileSignals);
        for (int i = 0; i < fullUser.getPositions().size(); i++) {
            LinkedinPosition position = fullUser.getPositions().get(i);
            LinkedinSignalPosition signal = new LinkedinSignalPosition(position, fullUser.getUserId(), i);
            addSignal(signal, signals);
        }
        for (int i = 0; i < fullUser.getSkills().size(); i++) {
            String skill = fullUser.getSkills().get(i);
            LinkedinSignalSkill signal = new LinkedinSignalSkill(skill, fullUser.getUserId(), i);
            addSignal(signal, signals);
        }

        return new ArrayList<>(signals);
    }

    @Override
    public List<NetworkSignal<Linkedin>> extract(LinkedinFullPost fullPost) {

        Set<NetworkSignal<Linkedin>> signals = new HashSet<>();

        if (fullPost.getId().startsWith("UPDATE")) {
            LinkedinSignalShareAuthorship signal = new LinkedinSignalShareAuthorship(fullPost.getPost());
            addSignal(signal, signals);
        } else {
            LinkedinSignalPostAuthorship signal = new LinkedinSignalPostAuthorship(fullPost.getPost());
            addSignal(signal, signals);
        }
        for (LinkedinActionLike like : fullPost.getLikes()) {
            LinkedinSignalPostLike signal = new LinkedinSignalPostLike(fullPost.getPost(),
                    like.getAuthorId(), like.getCreationTimestamp());
            addSignal(signal, signals);
        }
        for (LinkedinActionComment comment : fullPost.getComments()) {
            LinkedinSignalPostComment signal = new LinkedinSignalPostComment(fullPost.getPost(),
                    comment.getAuthorId(), comment.getCreationTimestamp());
            addSignal(signal, signals);
        }

        return new ArrayList<>(signals);
    }

    public List<NetworkSignal<Linkedin>> extract(LinkedinGroup group,
                                                 String userId) {
        List<NetworkSignal<Linkedin>> signals = new ArrayList<>();
        LinkedinSignalGroupMembership signal = new LinkedinSignalGroupMembership(group, userId);
        addSignal(signal, signals);
        return signals;
    }

    private List<NetworkSignal<Linkedin>> extract(LinkedinProfile profile) {
        Set<NetworkSignal<Linkedin>> signals = new HashSet<>();

        if (profile.getHeadline() != null) {
            LinkedinSignalBioHeadline signal = new LinkedinSignalBioHeadline(profile);
            addSignal(signal, signals);
        }
        if (profile.getSummary() != null) {
            LinkedinSignalBioDescription signal = new LinkedinSignalBioDescription(profile);
            addSignal(signal, signals);
        }
        if (profile.getTitle() != null || profile.getPositionSummary() != null) {
            LinkedinPosition position = new LinkedinPosition();
            position.setTitle(profile.getTitle());
            position.setEnd("present");
            if (profile.getPositionSummary() != null) {
                position.setDescription(profile.getPositionSummary());
            }
            if (profile.getCompany() != null) {
                position.setOrganizationName(profile.getCompany());
            }
            LinkedinSignalPosition signal = new LinkedinSignalPosition(position, profile.getId(), 0);
            addSignal(signal, signals);
        }
        return new ArrayList<>(signals);
    }

    private void addSignal(NetworkSignal<Linkedin> signal,
                           Collection<NetworkSignal<Linkedin>> signals) {
        if (signals.contains(signal)) {
            return;
        }
        Map<String, Keyword> keywords = nlpCore.extractKeywords(signal.getNlpAnalyzableContent());
        signal.getContentStems().clear();
        signal.getContentStems().addAll(keywords.keySet());
        signals.add(signal);
    }
}
