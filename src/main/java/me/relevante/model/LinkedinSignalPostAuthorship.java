package me.relevante.model;

import java.util.Date;

public class LinkedinSignalPostAuthorship extends AbstractLinkedinSignal implements NetworkSignal {

    public LinkedinSignalPostAuthorship() {
        super("PA", SignalCategory.CONTENT_ACTIVITY);
    }

    public LinkedinSignalPostAuthorship(LinkedinPost post) {
        this();
        this.nlpAnalyzableContent = post.getNlpAnalyzableContent();
        this.relatedEntityId = post.getId();
        this.relatedUserId = post.getAuthorId();
        this.signalTimestamp = (post.getCreationTimestamp() != null) ? post.getCreationTimestamp() : new Date();
    }
}
