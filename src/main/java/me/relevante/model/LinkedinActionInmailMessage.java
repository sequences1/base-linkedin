package me.relevante.model;

import me.relevante.network.Linkedin;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LinkedinActionInmailMessage")
public class LinkedinActionInmailMessage extends AbstractNetworkUserAction<Linkedin> implements NetworkUserAction<Linkedin> {

    private String subject;
	private String messageText;

	@Transient
	private LinkedinProfile author;
	@Transient
	private LinkedinProfile targetUser;

    public LinkedinActionInmailMessage() {
        super();
    }

    public LinkedinActionInmailMessage(String authorId,
                                       String targetUserId,
                                       String subject,
                                       String messageText) {
        super(authorId, targetUserId);
        this.subject = subject;
        this.messageText = messageText;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

	public String getSubject() {
		return subject;
	}

	public String getMessageText() {
		return messageText;
	}

    public LinkedinProfile getAuthor() {
		return author;
	}

    public void setAuthor(LinkedinProfile author) {
        this.author = author;
        this.authorId = author.getId();
    }

    public LinkedinProfile getTargetUser() {
		return targetUser;
	}

    public void setTargetUser(LinkedinProfile targetUser) {
        this.targetUser = targetUser;
        this.targetUserId = targetUser.getId();
    }
}
