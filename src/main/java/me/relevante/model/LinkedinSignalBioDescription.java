package me.relevante.model;

import java.util.Date;

public class LinkedinSignalBioDescription extends AbstractLinkedinSignal implements NetworkSignal {

    public LinkedinSignalBioDescription() {
        super("BD", SignalCategory.SELF_DESCRIPTION);
    }

    public LinkedinSignalBioDescription(LinkedinProfile profile) {
        this();
        this.nlpAnalyzableContent = profile.getSummary();
        this.relatedEntityId = profile.getId();
        this.relatedUserId = profile.getId();
        this.signalTimestamp = (profile.getUpdateTimestamp() != null) ? profile.getUpdateTimestamp() : new Date();
    }
}
