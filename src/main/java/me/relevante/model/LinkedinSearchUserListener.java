package me.relevante.model;

import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterConvertEvent;
import org.springframework.stereotype.Component;

@Component
public class LinkedinSearchUserListener extends AbstractMongoEventListener<LinkedinSearchUser> {

    @Override
    public void onAfterConvert(AfterConvertEvent<LinkedinSearchUser> event) {
        super.onAfterConvert(event);
        LinkedinSearchUser searchUser = event.getSource();
        for (NetworkSignal signal : searchUser.getSignals()) {
            signal.setRelatedUserId(searchUser.getId());
        }
    }
}
