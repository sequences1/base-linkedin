package me.relevante.model;

import java.util.Date;

public class LinkedinSignalCurrentPositionTitle extends AbstractLinkedinSignal implements NetworkSignal {

    public LinkedinSignalCurrentPositionTitle() {
        super("CPT", SignalCategory.SELF_DESCRIPTION);
    }

    public LinkedinSignalCurrentPositionTitle(LinkedinProfile profile) {
        this();
        this.nlpAnalyzableContent = profile.getTitle();
        this.relatedEntityId = profile.getId();
        this.relatedUserId = profile.getId();
        this.signalTimestamp = (profile.getUpdateTimestamp() != null) ? profile.getUpdateTimestamp() : new Date();
    }
}
