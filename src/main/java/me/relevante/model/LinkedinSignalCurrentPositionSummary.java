package me.relevante.model;

import java.util.Date;

public class LinkedinSignalCurrentPositionSummary extends AbstractLinkedinSignal implements NetworkSignal {

    public LinkedinSignalCurrentPositionSummary() {
        super("CPS", SignalCategory.SELF_DESCRIPTION);
    }

    public LinkedinSignalCurrentPositionSummary(LinkedinProfile profile) {
        this();
        this.nlpAnalyzableContent = profile.getPositionSummary();
        this.relatedEntityId = profile.getId();
        this.relatedUserId = profile.getId();
        this.signalTimestamp = (profile.getUpdateTimestamp() != null) ? profile.getUpdateTimestamp() : new Date();
    }
}
