package me.relevante.model;

import me.relevante.network.Linkedin;
import me.relevante.nlp.core.Keyword;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LinkedinPost implements NetworkPost<Linkedin, LinkedinPost, LinkedinProfile> {
	
    private String id;
    private String groupId;
    private String authorId;
    private String title;
    private String subTitle;
    private String content;
    private String subContent;
    private String url;
    private String imageUrl;
    private String language;
    private Date creationTimestamp;

    @Transient
    private List<LinkedinLike> likes;
    @Transient
    private List<LinkedinComment> comments;
    @Transient
    private LinkedinProfile author;
    @Transient
    private List<Keyword> keywords;

    public LinkedinPost() {
        super();
        this.likes = new ArrayList<>();
        this.comments = new ArrayList<>();
        this.keywords = new ArrayList<>();
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

	@Override
	public Date getCreationTimestamp() {
		return this.creationTimestamp;
	}

	@Override
	public void setCreationTimestamp(Date timestamp) {
		this.creationTimestamp = timestamp;
	}

    @Override
    public String getAuthorId() {
        return authorId;
    }

    @Override
    public LinkedinProfile getAuthor() {
        return author;
    }

    @Override
    public void setAuthor(LinkedinProfile author) {
        this.author = author;
        this.authorId = author.getId();
    }

    public List<Keyword> getKeywords() {
        return keywords;
    }

    @Override
    public String getNlpAnalyzableContent() {
        String analyzableContent = StringUtils.defaultString(content) + " t " +
                StringUtils.defaultString(subContent) + " t " +
                StringUtils.defaultString(title) + " t " +
                StringUtils.defaultString(subTitle) + " t ";
        return analyzableContent;
    }

    @Override
    public void completeDataFrom(LinkedinPost post) {
        updateDataWithReferenceObject(this, post);
    }

    @Override
    public void updateDataFrom(LinkedinPost post) {
        updateDataWithReferenceObject(post, this);
    }

    public String getLinkedinId() {
		return id;
	}

	public void setLinkedinId(String linkedinId) {
		this.id = linkedinId;
	}

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

    public String getSubContent() {
        return subContent;
    }

    public void setSubContent(String subContent) {
        this.subContent = subContent;
    }

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageURL) {
		this.imageUrl = imageURL;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

    private void updateDataWithReferenceObject(LinkedinPost referenceObject,
                                               LinkedinPost post) {
        if (!StringUtils.isBlank(referenceObject.id)) id = post.id;
        if (!StringUtils.isBlank(referenceObject.groupId)) groupId = post.groupId;
        if (!StringUtils.isBlank(referenceObject.authorId)) authorId = post.authorId;
        if (!StringUtils.isBlank(referenceObject.title)) title = post.title;
        if (!StringUtils.isBlank(referenceObject.subTitle)) subTitle = post.subTitle;
        if (!StringUtils.isBlank(referenceObject.content)) content = post.content;
        if (!StringUtils.isBlank(referenceObject.subContent)) subContent = post.subContent;
        if (referenceObject.creationTimestamp != null) creationTimestamp = post.creationTimestamp;
        if (!StringUtils.isBlank(referenceObject.url)) url = post.url;
        if (!StringUtils.isBlank(referenceObject.imageUrl)) imageUrl = post.imageUrl;
        if (!StringUtils.isBlank(referenceObject.language)) language = post.language;
    }

    public List<LinkedinLike> getLikes() {
        return likes;
    }

    public void setLikes(List<LinkedinLike> likes) {
        this.likes = new ArrayList<>(likes);
    }

    public List<LinkedinComment> getComments() {
        return comments;
    }

    public void setComments(List<LinkedinComment> comments) {
        this.comments = new ArrayList<>(comments);
    }

    public LinkedinLike findLikeByAuthorId(String authorId) {
        if (authorId == null) {
            return null;
        }
        for (LinkedinLike like : this.likes) {
            if (authorId.equals(like.getAuthorId())) {
                return like;
            }
        }
        return null;
    }

    public LinkedinComment findCommentByAuthorId(String authorId) {
        if (authorId == null) {
            return null;
        }
        for (LinkedinComment comment : this.comments) {
            if (authorId.equals(comment.getAuthorId())) {
                return comment;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "LinkedinPost{" +
                "id='" + id + '\'' +
                ", groupId='" + groupId + '\'' +
                ", authorId='" + authorId + '\'' +
                ", title='" + title + '\'' +
                ", subTitle='" + subTitle + '\'' +
                ", content='" + content + '\'' +
                ", subContent='" + subContent + '\'' +
                ", creationTimestamp=" + creationTimestamp +
                ", url='" + url + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", language='" + language + '\'' +
                ", likes=" + likes +
                ", comments=" + comments +
                ", author=" + author +
                '}';
    }
}
