package me.relevante.model;

import me.relevante.network.Linkedin;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "LinkedinFullUser")
public class LinkedinFullUser implements NetworkFullUser<Linkedin, LinkedinProfile>, NetworkPostsContainer<Linkedin, LinkedinFullPost>, Updatable<LinkedinFullUser> {

    @Id
    private String id;
    private LinkedinProfile profile;
    private List<String> postIds;
    private List<String> groupIds;
    private List<String> skills;
    private List<LinkedinPosition> positions;
    private List<String> relatedTerms;

    @Transient
    private List<LinkedinActionConnectRequest> connectRequests;
    @Transient
    private List<LinkedinActionInmailMessage> inmailMessages;
    @Transient
    protected List<LinkedinFullPost> lastPosts;
    @Transient
    protected List<LinkedinGroup> groups;

    public LinkedinFullUser() {
        super();
        this.postIds = new ArrayList<>();
        this.groupIds = new ArrayList<>();
        this.skills = new ArrayList<>();
        this.positions = new ArrayList<>();
        this.relatedTerms = new ArrayList<>();
        this.connectRequests = new ArrayList<>();
        this.inmailMessages = new ArrayList<>();
        this.lastPosts = new ArrayList<>();
        this.groups = new ArrayList<>();
    }

    public LinkedinFullUser(LinkedinProfile profile) {
        this();
        this.id = profile.getId();
        this.profile = profile;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public LinkedinProfile getProfile() {
        return profile;
    }

    @Override
    public List<String> getRelatedTerms() {
        return relatedTerms;
    }

    @Override
    public String getUserId() {
        return profile.getId();
    }

    @Override
    public List<LinkedinFullPost> getLastPosts() {
        return lastPosts;
    }

    @Override
    public void updateDataFrom(LinkedinFullUser fullUser) {
        this.profile.updateDataFrom(fullUser.getProfile());
        this.id = fullUser.getId();
        this.relatedTerms.clear();
        this.relatedTerms.addAll(fullUser.getRelatedTerms());
        this.positions.clear();
        this.positions.addAll(fullUser.getPositions());
        this.skills.clear();
        this.skills.addAll(fullUser.getSkills());
        this.groupIds.clear();
        this.groupIds.addAll(fullUser.getGroupIds());
        this.postIds.clear();
        this.postIds.addAll(fullUser.getPostIds());
    }

    @Override
    public void completeDataFrom(LinkedinFullUser fullUser) {
        this.profile.completeDataFrom(fullUser.getProfile());
        this.id = (id == null) ? fullUser.getId() : id;
        completeWithMissingElements(this.relatedTerms, fullUser.getRelatedTerms());
        completeWithMissingElements(this.positions, fullUser.getPositions());
        completeWithMissingElements(this.skills, fullUser.getSkills());
        completeWithMissingElements(this.groupIds, fullUser.getGroupIds());
        completeWithMissingElements(this.postIds, fullUser.getPostIds());
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getPostIds() {
        return postIds;
    }

    public List<String> getGroupIds() {
        return groupIds;
    }

    public List<String> getSkills() {
        return skills;
    }

    public List<LinkedinPosition> getPositions() {
        return positions;
    }

    public List<LinkedinGroup> getGroups() {
        return groups;
    }

    public List<LinkedinActionConnectRequest> getConnectRequests() {
        return connectRequests;
    }

    public List<LinkedinActionInmailMessage> getInmailMessages() {
        return inmailMessages;
    }

    public List<LinkedinActionInmailMessage> findInmailMessagesByAuthorId(String authorId) {
        List<LinkedinActionInmailMessage> inmailMessages = new ArrayList<>();
        if (authorId == null) {
            return inmailMessages;
        }
        for (LinkedinActionInmailMessage linkedinInmailMessage : this.inmailMessages) {
            if (authorId.equals(linkedinInmailMessage.getAuthorId())) {
                inmailMessages.add(linkedinInmailMessage);
            }
        }
        return inmailMessages;
    }

    public List<LinkedinActionConnectRequest> findConnectRequestsByAuthorId(String authorId) {
        List<LinkedinActionConnectRequest> connectRequests = new ArrayList<>();
        if (authorId == null) {
            return connectRequests;
        }
        for (LinkedinActionConnectRequest linkedinConnectRequest : this.connectRequests) {
            if (authorId.equals(linkedinConnectRequest.getAuthorId())) {
                connectRequests.add(linkedinConnectRequest);
            }
        }
        return connectRequests;
    }

    private void completeWithMissingElements(List listToBeCompleted,
                                            List listToCompleteWith) {
        for (Object object : listToCompleteWith) {
            if (!listToBeCompleted.contains(object)) {
                listToBeCompleted.add(object);
            }
        }
    }
}
