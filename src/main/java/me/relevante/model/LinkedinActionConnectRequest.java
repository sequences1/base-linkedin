package me.relevante.model;

import me.relevante.network.Linkedin;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LinkedinActionConnectRequest")
public class LinkedinActionConnectRequest extends AbstractNetworkUserAction<Linkedin> implements NetworkUserAction<Linkedin> {

    @Transient
    private LinkedinProfile author;
    @Transient
    private LinkedinProfile targetUser;

    public LinkedinActionConnectRequest() {
        super();
    }

    public LinkedinActionConnectRequest(String authorId,
                                        String targetUserId) {
        super(authorId, targetUserId);
        this.authorId = authorId;
        this.targetUserId = targetUserId;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public LinkedinProfile getAuthor() {
        return author;
    }

    public void setAuthor(LinkedinProfile author) {
        this.author = author;
        this.authorId = author.getId();
    }

    public LinkedinProfile getTargetUser() {
        return targetUser;
    }

    public void setTargetUser(LinkedinProfile targetUser) {
        this.targetUser = targetUser;
        this.targetUserId = targetUser.getId();
    }
}
