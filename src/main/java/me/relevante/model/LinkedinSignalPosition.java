package me.relevante.model;

public class LinkedinSignalPosition extends AbstractLinkedinSignal implements NetworkSignal {

    public LinkedinSignalPosition() {
        super("BP", SignalCategory.SELF_DESCRIPTION);
    }

    public LinkedinSignalPosition(LinkedinPosition position,
                                  String userId,
                                  int signalIndex) {
        this();
        this.nlpAnalyzableContent = position.getNlpAnalyzableContent();
        this.relatedEntityId = String.valueOf(signalIndex);
        this.relatedUserId = userId;
        this.signalTimestamp = position.getEndDate();
    }
}
