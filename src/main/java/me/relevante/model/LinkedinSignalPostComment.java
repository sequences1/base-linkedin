package me.relevante.model;

import java.util.Date;

public class LinkedinSignalPostComment extends AbstractLinkedinSignal implements NetworkSignal {

    public LinkedinSignalPostComment() {
        super("PC", SignalCategory.CONTENT_ACTIVITY);
    }

    public LinkedinSignalPostComment(LinkedinPost post,
                                     String commentAuthorId,
                                     Date commentTimestamp) {
        this();
        this.nlpAnalyzableContent = post.getNlpAnalyzableContent();
        this.relatedEntityId = post.getId();
        this.relatedUserId = commentAuthorId;
        this.signalTimestamp = (commentTimestamp != null) ? commentTimestamp : new Date();
    }
}
