package me.relevante.model;

import me.relevante.network.Linkedin;
import me.relevante.nlp.core.Keyword;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Transient;

import java.util.Date;
import java.util.List;

public class LinkedinProfile implements NetworkProfile<Linkedin, LinkedinProfile> {

    protected String id;
    protected String name;
    protected String firstName;
    protected String lastName;
    protected String email;
    protected String headline;
    protected String profileUrl;
    protected String imageUrl;
    protected String area;
    protected String country;
    protected String countryCode;
    protected String summary;
    protected String industry;
    protected String title;
    protected String positionSummary;
    protected String company;
    protected String numberConnections;
    protected Date updateTimestamp;

    @Transient
    protected String location;
    @Transient
    protected List<Keyword> keywords;
    @Transient
    protected LinkedinPost currentShare;

    public LinkedinProfile() {
        super();
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getUserId() {
        return this.id;
    }

    public List<Keyword> getKeywords() {
        return keywords;
    }

    @Override
    public String getNlpAnalyzableContent() {
        String analyzableContent = StringUtils.defaultString(company) + " t " +
                StringUtils.defaultString(headline) + " t " +
                StringUtils.defaultString(summary) + " t " +
                StringUtils.defaultString(industry) + " t " +
                StringUtils.defaultString(positionSummary) + " t " +
                StringUtils.defaultString(title) + " t ";
        return analyzableContent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = (headline != null) ? headline : "";
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPositionSummary() {
        return positionSummary;
    }

    public void setPositionSummary(String positionSummary) {
        this.positionSummary = positionSummary;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getNumberConnections() {
        return numberConnections;
    }

    public void setNumberConnections(String numberConnections) {
        this.numberConnections = numberConnections;
    }

    public Date getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Date updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public LinkedinPost getCurrentShare() {
        return currentShare;
    }

    public void setCurrentShare(LinkedinPost currentShare) {
        this.currentShare = currentShare;
    }

    @Override
    public void completeDataFrom(LinkedinProfile user) {
        updateDataWithReferenceObject(this, user);
    }

    @Override
    public void updateDataFrom(LinkedinProfile user) {
        updateDataWithReferenceObject(user, this);
    }

    private void updateDataWithReferenceObject(LinkedinProfile referenceObject,
                                               LinkedinProfile user) {
        if (!StringUtils.isBlank(referenceObject.id)) id = user.id;
        if (!StringUtils.isBlank(referenceObject.name)) name = user.name;
        if (!StringUtils.isBlank(referenceObject.firstName)) firstName = user.firstName;
        if (!StringUtils.isBlank(referenceObject.lastName)) lastName = user.lastName;
        if (!StringUtils.isBlank(referenceObject.email)) email = user.email;
        if (!StringUtils.isBlank(referenceObject.headline)) headline = user.headline;
        if (!StringUtils.isBlank(referenceObject.profileUrl)) profileUrl = user.profileUrl;
        if (!StringUtils.isBlank(referenceObject.imageUrl)) imageUrl = user.imageUrl;
        if (!StringUtils.isBlank(referenceObject.location)) location = user.location;
        if (!StringUtils.isBlank(referenceObject.area)) area = user.area;
        if (!StringUtils.isBlank(referenceObject.country)) country = user.country;
        if (!StringUtils.isBlank(referenceObject.countryCode)) countryCode = user.countryCode;
        if (!StringUtils.isBlank(referenceObject.summary)) summary = user.summary;
        if (!StringUtils.isBlank(referenceObject.industry)) industry = user.industry;
        if (!StringUtils.isBlank(referenceObject.title)) title = user.title;
        if (!StringUtils.isBlank(referenceObject.positionSummary)) positionSummary = user.positionSummary;
        if (!StringUtils.isBlank(referenceObject.company)) company = user.company;
        if (referenceObject.updateTimestamp != null) updateTimestamp = user.updateTimestamp;
    }

    @Override
    public String toString() {
        return "LinkedinProfile{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", headline='" + headline + '\'' +
                ", profileUrl='" + profileUrl + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", area='" + area + '\'' +
                ", country='" + country + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", summary='" + summary + '\'' +
                ", industry='" + industry + '\'' +
                ", title='" + title + '\'' +
                ", positionSummary='" + positionSummary + '\'' +
                ", company='" + company + '\'' +
                ", location='" + location + '\'' +
                ", updateTimestamp='" + updateTimestamp + '\'' +
                '}';
    }
}