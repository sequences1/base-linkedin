package me.relevante.model;

import me.relevante.network.Linkedin;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LinkedinActionLike")
public class LinkedinActionLike extends AbstractNetworkPostAction<Linkedin> implements NetworkPostAction<Linkedin> {

    @Transient
    private LinkedinProfile author;

    public LinkedinActionLike() {
        super();
    }

    public LinkedinActionLike(String authorId,
                              String postId) {
        super(authorId, postId);
    }

    public LinkedinActionLike(LinkedinProfile author,
                              String postId) {
        this(author.getId(), postId);
        this.setAuthor(author);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public LinkedinProfile getAuthor() {
        return author;
    }

    public void setAuthor(LinkedinProfile author) {
        this.author = author;
        this.authorId = author.getId();
    }
}
