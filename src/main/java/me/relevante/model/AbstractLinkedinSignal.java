package me.relevante.model;

import me.relevante.network.Linkedin;
import me.relevante.network.Network;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class AbstractLinkedinSignal implements NetworkSignal {

    protected String relatedEntityId;
    protected List<String> contentStems;
    protected Date signalTimestamp;

    @Transient protected String nlpAnalyzableContent;
    @Transient protected String relatedUserId;
    @Transient protected String keySignalPrefix;
    @Transient protected SignalCategory category;

    public AbstractLinkedinSignal() {
        this.contentStems = new ArrayList<>();
    }

    public AbstractLinkedinSignal(String keySignalPrefix,
                                  SignalCategory category) {
        this();
        this.keySignalPrefix = keySignalPrefix;
        this.category = category;
    }

    @Override
    public Network getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public String getKey() {
        return keySignalPrefix + "-" + relatedEntityId + "-" + relatedUserId;
    }

    @Override
    public SignalCategory getCategory() {
        return category;
    }

    @Override
    public String getRelatedUserId() {
        return relatedUserId;
    }

    @Override
    public void setRelatedUserId(String relatedUserId) {
        this.relatedUserId = relatedUserId;
    }

    @Override
    public String getRelatedEntityId() {
        return relatedEntityId;
    }

    @Override
    public String getNlpAnalyzableContent() {
        return nlpAnalyzableContent;
    }

    public void setNlpAnalyzableContent(String nlpAnalyzableContent) {
        this.nlpAnalyzableContent = nlpAnalyzableContent;
    }

    @Override
    public Date getSignalTimestamp() {
        return signalTimestamp;
    }

    public void setSignalTimestamp(Date signalTimestamp) {
        this.signalTimestamp = signalTimestamp;
    }

    @Override
    public List<String> getContentStems() {
        return contentStems;
    }

    @Override
    public boolean equals(Object o) {
        AbstractLinkedinSignal that = (AbstractLinkedinSignal) o;
        return (that.getKey().equals(this.getKey()));

    }

    @Override
    public int hashCode() {
        return getKey().hashCode();
    }
}
