package me.relevante.model;

import java.util.Date;

public class LinkedinSignalGroupMembership extends AbstractLinkedinSignal implements NetworkSignal {

    public LinkedinSignalGroupMembership() {
        super("GM", SignalCategory.OWNERSHIP);
    }

    public LinkedinSignalGroupMembership(LinkedinGroup group,
                                         String userId) {
        this();
        this.nlpAnalyzableContent = group.getNlpAnalyzableContent();
        this.relatedEntityId = group.getId();
        this.relatedUserId = userId;
        this.signalTimestamp = new Date();
    }

    public void setRelatedEntity(LinkedinGroup linkedinGroup) {
        this.relatedEntityId = linkedinGroup.getId();
    }

}
