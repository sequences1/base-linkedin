package me.relevante.model;

import me.relevante.network.Linkedin;
import me.relevante.network.Network;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "LinkedinFullPost")
public class LinkedinFullPost implements NetworkFullPost<Linkedin, LinkedinPost> {

    @Id
    private String id;
    private LinkedinPost post;

    @Transient
    private List<LinkedinActionLike> likes;
    @Transient
    private List<LinkedinActionComment> comments;
    @Transient
    private LinkedinGroup group;

    public LinkedinFullPost() {
        super();
        this.likes = new ArrayList<>();
        this.comments = new ArrayList<>();
    }

    public LinkedinFullPost(LinkedinPost post) {
        this();
        this.id = post.getId();
        this.post = post;
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public LinkedinPost getPost() {
        return post;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        LinkedinFullPost that = (LinkedinFullPost) obj;
        return this.id.equals(that.id);
    }

    public LinkedinGroup getGroup() {
        return group;
    }

    public void setGroup(LinkedinGroup group) {
        this.group = group;
    }

    public List<LinkedinActionLike> getLikes() {
        return likes;
    }

    public void setLikes(List<LinkedinActionLike> likes) {
        this.likes = new ArrayList<>(likes);
    }

    public List<LinkedinActionComment> getComments() {
        return comments;
    }

    public void setComments(List<LinkedinActionComment> comments) {
        this.comments = new ArrayList<>(comments);
    }

    public List<LinkedinActionLike> findLikesByAuthorId(String authorId) {
        List<LinkedinActionLike> likes = new ArrayList<>();
        if (authorId == null) {
            return likes;
        }
        for (LinkedinActionLike like : this.likes) {
            if (authorId.equals(like.getAuthorId())) {
                likes.add(like);
            }
        }
        return likes;
    }

    public List<LinkedinActionComment> findCommentsByAuthorId(String authorId) {
        List<LinkedinActionComment> comments = new ArrayList<>();
        if (authorId == null) {
            return comments;
        }
        for (LinkedinActionComment comment : this.comments) {
            if (authorId.equals(comment.getAuthorId())) {
                comments.add(comment);
            }
        }
        return comments;
    }

}
