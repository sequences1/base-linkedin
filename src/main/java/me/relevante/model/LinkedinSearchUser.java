package me.relevante.model;

import me.relevante.network.Linkedin;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "LinkedinSearchUser")
public class LinkedinSearchUser implements NetworkSearchUser<Linkedin, LinkedinFullUser, LinkedinFullPost> {

    @Id
    private String id;
    private List<NetworkSignal<Linkedin>> signals;

    @Transient private LinkedinFullUser fullUser;
    @Transient private List<LinkedinFullPost> posts;
    @Transient private List<LinkedinGroup> groups;

    public LinkedinSearchUser(LinkedinFullUser fullUser) {
        this();
        this.setFullUser(fullUser);
    }

    private LinkedinSearchUser() {
        this.signals = new ArrayList<>();
        this.posts = new ArrayList<>();
        this.groups = new ArrayList<>();
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getUserId() {
        return getId();
    }

    @Override
    public LinkedinFullUser getFullUser() {
        return fullUser;
    }

    @Override
    public void setFullUser(LinkedinFullUser fullUser) {
        this.id = fullUser.getId();
        this.fullUser = fullUser;
    }

    @Override
    public List<LinkedinFullPost> getPosts() {
        return posts;
    }

    @Override
    public void addSignal(NetworkSignal signal) {
        if (signals.contains(signal)) {
            return;
        }
        this.signals.add(signal);
    }

    @Override
    public List<NetworkSignal<Linkedin>> getSignals() {
        return signals;
    }

    public List<LinkedinGroup> getGroups() {
        return groups;
    }
}
