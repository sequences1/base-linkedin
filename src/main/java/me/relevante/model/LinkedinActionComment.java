package me.relevante.model;

import me.relevante.network.Linkedin;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "LinkedinActionComment")
public class LinkedinActionComment extends AbstractNetworkPostAction<Linkedin> implements NetworkPostAction<Linkedin> {

	private String comment;

	@Transient
	private LinkedinProfile author;

    public LinkedinActionComment() {
        super();
    }

    public LinkedinActionComment(String authorId,
                                 String postId,
                                 String comment) {
        super(authorId, postId);
		this.comment = comment;
	}

    public LinkedinActionComment(LinkedinProfile author,
                                 String postId,
                                 String comment) {
        super(author.getId(), postId);
        this.comment = comment;
        setAuthor(author);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public String getComment() {
        return comment;
    }

    public LinkedinProfile getAuthor() {
        return author;
    }

    public void setAuthor(LinkedinProfile author) {
        this.author = author;
        this.authorId = author.getId();
    }
}
