package me.relevante.model;

import java.util.Date;

public class LinkedinSignalPostLike extends AbstractLinkedinSignal implements NetworkSignal {

    public LinkedinSignalPostLike() {
        super("PL", SignalCategory.CONTENT_ACTIVITY);
    }

    public LinkedinSignalPostLike(LinkedinPost post,
                                  String likeAuthorId,
                                  Date likeTimestamp) {
        this();
        this.nlpAnalyzableContent = post.getNlpAnalyzableContent();
        this.relatedEntityId = post.getId();
        this.relatedUserId = likeAuthorId;
        this.signalTimestamp = (likeTimestamp != null) ? likeTimestamp : new Date();
    }
}

