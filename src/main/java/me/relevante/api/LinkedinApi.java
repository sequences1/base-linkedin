package me.relevante.api;

import me.relevante.model.LinkedinPost;
import me.relevante.model.LinkedinProfile;
import me.relevante.network.Linkedin;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Reader;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.Date;

public class LinkedinApi implements NetworkApi<Linkedin, LinkedinProfile> {

    private static final Logger logger = LoggerFactory.getLogger(LinkedinApi.class);

    private static final String ID_TOKEN = "$$$id$$$";
    private static final String URL_TOKEN = "$$$url$$$";
    private static final String PROFILE_FIELDS = "(id,first-name,last-name,picture-url,public-profile-url,email-address,headline,current-share,location,summary,industry,group-memberships,primary-twitter-account,twitter-accounts,positions:(id,title,summary,start-date,end-date,is-current,company:(id,name,type,size,industry,ticker)))";
    private static final String PROFILE_BY_ID_REQUEST = "https://api.linkedin.com/v1/people/id=" + ID_TOKEN + ":" + PROFILE_FIELDS;
    private static final String PROFILE_BY_URL_REQUEST = "https://api.linkedin.com/v1/people/url=" + URL_TOKEN + ":" + PROFILE_FIELDS;

    private OAuthService oAuthService;
    private OAuthTokenPair oAuthTokenPair;

    public LinkedinApi(OAuthKeyPair oAuthConsumerKeyPair,
                       OAuthTokenPair oAuthAccessTokenPair) {
        this.oAuthTokenPair = oAuthAccessTokenPair;
        this.oAuthService = createOAuthService(oAuthConsumerKeyPair);
    }

    @Override
    public Linkedin getNetwork() {
        return Linkedin.getInstance();
    }

    public LinkedinProfile getUserData() {
        String profileUrl = PROFILE_BY_ID_REQUEST.replace("id=" + ID_TOKEN, "~");
        return getUserDataFromUrl(profileUrl);
    }

    public LinkedinProfile getUserDataByPublicUrl(String publicUrl) {
        String encodedUrl = URLEncoder.encode(publicUrl);
        String profileUrl = PROFILE_BY_URL_REQUEST.replace(URL_TOKEN, encodedUrl);
        return getUserDataFromUrl(profileUrl);
    }

	private OAuthService createOAuthService(OAuthKeyPair oAuthConsumerKeyPair) {
        OAuthService oAuthService = new ServiceBuilder()
                .provider(LinkedInApi.class)
                .apiKey(oAuthConsumerKeyPair.getKey())
                .apiSecret(oAuthConsumerKeyPair.getSecret())
                .build();
        return oAuthService;
    }

    private LinkedinProfile getUserDataFromUrl(String url) {
        try {
            Token oAuthToken = new Token(oAuthTokenPair.getToken(), oAuthTokenPair.getSecret());
            OAuthRequest oAuthRequest = new OAuthRequest(Verb.GET, url);
            oAuthService.signRequest(oAuthToken, oAuthRequest);
            Response response = oAuthRequest.send();
            SAXBuilder builder = new SAXBuilder();
            Reader in = new StringReader(response.getBody());
            Document document = builder.build(in);
            Element profileNode = document.getRootElement();
            LinkedinProfile linkedinUser = createProfileFromProfileNode(profileNode);
            return linkedinUser;
        }
        catch (Exception e) {
            logger.error("Error", e);
        }
        return null;

    }

    private LinkedinProfile createProfileFromProfileNode(Element profileNode) {

        if (profileNode == null)
            return null;
        if (profileNode.getChildText("id") == null || profileNode.getChildText("id").equals("private"))
            return null;
        LinkedinProfile user = new LinkedinProfile();
        user.setFirstName(profileNode.getChildText("first-name"));
        user.setLastName(profileNode.getChildText("last-name"));
        user.setName(user.getFirstName() + " " + user.getLastName());
        user.setImageUrl(profileNode.getChildText("picture-url"));
        user.setProfileUrl(profileNode.getChildText("public-profile-url"));
        user.setId(composeUrlBasedId(user.getProfileUrl()));
        user.setEmail(profileNode.getChildText("email-address"));
        user.setHeadline(profileNode.getChildText("headline"));
        user.setPositionSummary(profileNode.getChildText("summary"));
        user.setIndustry(profileNode.getChildText("industry"));
        user.setUpdateTimestamp(new Date());

        Element shareNode = profileNode.getChild("current-share");
        if (shareNode != null) {
            Element contentNode = shareNode.getChild("content");
            LinkedinPost post = new LinkedinPost();
            user.setCurrentShare(post);
            post.setLinkedinId(shareNode.getChildText("id"));
            post.setCreationTimestamp(new Date(Long.parseLong(shareNode.getChildText("timestamp"))));
            post.setUrl(contentNode.getChildText("resolved-url"));
            post.setAuthor(user);
            post.setAuthorId(user.getId());
            post.setTitle(contentNode.getChildText("title"));
            post.setContent(contentNode.getChildText("description"));
            post.setImageUrl(contentNode.getChildText("submitted-image-url"));
        }

        Element locationNode = profileNode.getChild("location");
        if (locationNode != null) {
            user.setArea(locationNode.getChildText("name"));
            Element countryNode = locationNode.getChild("country");
            user.setCountryCode(countryNode.getChildText("code"));
        }

        return user;
    }

    private String composeUrlBasedId(String publicUrl) {

        int publicUrlIdIndex = publicUrl.indexOf("/in/");
        if (publicUrlIdIndex < 0) {
            publicUrlIdIndex = publicUrl.indexOf("/pub/");
        }
        if (publicUrlIdIndex < 0) {
            throw new IllegalArgumentException("Public URL not valid");
        }
        String publicUrlId = publicUrl.substring(publicUrlIdIndex + 1);
        return publicUrlId;
    }

}
